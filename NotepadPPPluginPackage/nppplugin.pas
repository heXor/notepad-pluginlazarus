{
    This file is part of DBGP Plugin for Notepad++
    Copyright (C) 2007  Damjan Zobo Cvetko
    Converted from Delphi to Lazarus in 2012 by
    Ludo Brands and Reinier Olislagers

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
}

unit nppplugin;

{$MODE Delphi}

interface

uses
  LCLIntf, LCLType, SciSupport,
  SysUtils, Dialogs,Classes,Forms, Windows,
  NPPTypes;

type
  { TNppPlugin }
  TNppPlugin = class(TObject)
  private
    FuncArray  : array of _TFuncItem;
    FNppData   : TNppData;
    FPluginName: nppString;

  protected
    // hooks
    procedure DoNppnToolbarModification; virtual;
    procedure DoNppnShutdown; virtual;

    function GetPluginsConfigDir: nppString;
    function GetSourceFilenameNoPath: nppString;
    function GetSourceFilename: nppString;


    // Add a new menu function to the plugin
    function AddFunction(Name: nppstring; const Func: PFUNCPLUGINCMD = nil; const ShortcutKey: AnsiChar = #0; const Shift: TShiftState = []): integer;
    procedure SetToolbarIcon(out pToolbarIcon: TToolbarIcons); virtual;

  public
    constructor Create; reintroduce; virtual;
    destructor Destroy; override;
    procedure BeforeDestruction; override;

    function CmdIdFromDlgId(DlgId: Integer): Integer;
    function SendToNpp(Msg:UINT; wParam:WPARAM; lParam:LPARAM):LRESULT;
    function SendToNppScintilla(Msg:UINT; wParam:WPARAM; lParam:LPARAM):LRESULT;
    function isMessageForPlugin(const pWnd: HWND): boolean;

    function GetStringNPP(pMsg: UInt; Len: DWORD = 4096): nppString;
    function GetStringSCI(pMsg: UInt; Len: DWORD = 4096): nppString;

    // needed for DLL export... wrappers are in the main dll source
    procedure SetInfo(pNppData: TNppData); virtual;
    function  GetName: nppPChar;
    function  GetFuncsArray(var FuncsCount: Integer): Pointer;
    procedure BeNotified(sn: PSCNotification); virtual;
    procedure MessageProc(var Msg: TMessage); virtual;

    // menu & tool bar mode
    procedure SetMenuItemCheck(CmdId: DWORD; Check: boolean);

    // df
    function DoOpen(filename: nppString): boolean; overload;
    function DoOpen(filename: nppString; Line: Integer): boolean; overload;

    property NppData: TNppData read FNppData;
    property PluginName: nppString read FPluginName write FPluginName;
  end;

  TNPPPLuginClass = class of TNppPlugin;

  // Global functions to set up the plugin
  procedure NPPInitialize(const pPluginClass: TNPPPLuginClass);
  function  GetNPPPluginInstance: TNPPPlugin;
  procedure DLLEntryPoint(dwReason: DWord);

  // DLL exported functions so use cdecl and export.
  procedure setInfo(NppData: TNppData); cdecl; export;
  function  getName(): nppPchar; cdecl; export;
  function  getFuncsArray(var nFuncs:integer):Pointer;cdecl; export;
  procedure beNotified(sn: PSCNotification); cdecl; export;
  function  messageProc(msg: Integer; _wParam: WPARAM; _lParam: LPARAM): LRESULT; cdecl; export;
  {$IFDEF NPPUNICODE}
  function  isUnicode : Boolean; cdecl; export;
  {$ENDIF}

implementation

// Handle to the plugin instance ('singleton')
var
  NPPPluginInstance: TNPPPlugin;

function GetNPPPluginInstance: TNPPPlugin;
begin
  result := NPPPluginInstance;
end;

procedure NPPInitialize(const pPluginClass: TNPPPLuginClass);
begin
  Dll_Process_Detach_Hook:= @DLLEntryPoint;
  NPPPluginInstance := pPluginClass.Create;
  DLLEntryPoint(DLL_PROCESS_ATTACH);
end;

procedure setInfo(NppData: TNppData); cdecl; export;
begin
  NPPPluginInstance.SetInfo(NppData);
end;

function getName(): nppPchar; cdecl; export;
begin
  Result := NPPPluginInstance.GetName;
end;

function getFuncsArray(var nFuncs:integer):Pointer;cdecl; export;
begin
  Result := NPPPluginInstance.GetFuncsArray(nFuncs);
end;

procedure beNotified(sn: PSCNotification); cdecl; export;
begin
  NPPPluginInstance.BeNotified(sn);
end;

function messageProc(msg: Integer; _wParam: WPARAM; _lParam: LPARAM): LRESULT; cdecl; export;
var xmsg:TMessage;
begin
  xmsg.Msg := msg;
  xmsg.WParam := _wParam;
  xmsg.LParam := _lParam;
  xmsg.Result := 0;
  NPPPluginInstance.MessageProc(xmsg);
  Result := xmsg.Result;
end;

{$IFDEF NPPUNICODE}
function isUnicode : Boolean; cdecl; export;
begin
  Result := true;
end;
{$ENDIF}

procedure DLLEntryPoint(dwReason: DWord);
begin
  case dwReason of
  DLL_PROCESS_ATTACH:
  begin
    // create the main object
    //Npp := TDbgpNppPlugin.Create;
  end;
  DLL_PROCESS_DETACH:
  begin
    NPPPluginInstance.Destroy;
  end;
  //DLL_THREAD_ATTACH: MessageBeep(0);
  //DLL_THREAD_DETACH: MessageBeep(0);
  end;
end;


{ TNppPlugin }

{ This is hacking for trouble...
  We need to unset the Application handler so that the forms
  don't get berserk and start throwing OS error 1004.
  This happens because the main NPP HWND is already lost when the
  DLL_PROCESS_DETACH gets called, and the form tries to allocate a new
  handler for sending the "close" windows message...
}
procedure TNppPlugin.BeforeDestruction;
begin
  //Application.Handle := 0;
  Application.Terminate;
  inherited;
end;

constructor TNppPlugin.Create;
begin
  inherited;
  PluginName := '<unknown>';
end;

destructor TNppPlugin.Destroy;
var i: Integer;
begin
  for i := 0 to Length(FuncArray)-1 do
    if assigned(FuncArray[i].ShortcutKey) then
      Dispose(FuncArray[i].ShortcutKey);
  inherited;
end;

function TNppPlugin.SendToNpp(Msg: UINT; wParam: WPARAM; lParam: LPARAM): LRESULT;
begin
  result := SendMessage(NppData.NppHandle, Msg, wParam, lParam);
end;

function TNppPlugin.SendToNppScintilla(Msg: UINT; wParam: WPARAM; lParam: LPARAM): LRESULT;
begin
  result := SendMessage(NppData.ScintillaMainHandle, Msg, wParam, lParam);
end;

function TNppPlugin.isMessageForPlugin(const pWnd: HWND): boolean;
begin
  result := pWnd = NppData.NppHandle;
end;

function TNppPlugin.AddFunction(Name: nppstring; const Func: PFUNCPLUGINCMD;
  const ShortcutKey: AnsiChar; const Shift: TShiftState): integer;
var
  NF: _TFuncItem;
begin
  // Set up the new function
  FillChar(NF, sizeof(NF), 0);
  {$IFDEF NPPUNICODE}
    StringToWideChar(Name, NF.ItemName, FuncItemNameLen); // @todo: change to constant
  {$ELSE}
    StrCopy(NF.ItemName, nppPChar(Name));
  {$ENDIF}
  NF.Func := Func;

  if ShortcutKey <> #0 then
  begin
    New(NF.ShortcutKey);
    NF.ShortcutKey.IsCtrl  := ssCtrl  in Shift;
    NF.ShortcutKey.IsAlt   := ssAlt   in Shift;
    NF.ShortcutKey.IsShift := ssShift in Shift;
    NF.ShortcutKey.Key     := ShortcutKey; // need widechar ??
  end;

  // Add the new function to the list
  SetLength(FuncArray, Length(FuncArray) + 1);
  FuncArray[Length(FuncArray) - 1] := NF;   // Zero-based so -1
  Result := Length(FuncArray) - 1;
end;

function TNppPlugin.GetFuncsArray(var FuncsCount: Integer): Pointer;
begin
  FuncsCount := Length(FuncArray);
  Result := FuncArray;
end;

function TNppPlugin.GetName: nppPChar;
begin
  Result := nppPChar(PluginName);
end;

function TNppPlugin.GetPluginsConfigDir: nppString;
begin
  result := GetStringNPP(NPPM_GETPLUGINSCONFIGDIR);
end;

function TNppPlugin.GetSourceFilenameNoPath: nppString;
begin
  result := GetStringNPP(NPPM_GETFILENAME);
end;

function TNppPlugin.GetSourceFilename: nppString;
begin
  result := GetStringNPP(NPPM_GETFULLCURRENTPATH)
end;

procedure TNppPlugin.BeNotified(sn: PSCNotification);
begin
  // Message for the plugin?
  if HWND(sn^.nmhdr.hwndFrom) = NppData.NppHandle then
    case sn^.nmhdr.code of
      NPPN_TB_MODIFICATION: DoNppnToolbarModification;
      NPPN_SHUTDOWN       : DoNppnShutdown;
    end;
  // @todo
end;

procedure TNppPlugin.MessageProc(var Msg: TMessage);
var
  hm: HMENU;
  i: integer;
begin
  if Msg.Msg = WM_CREATE then
  begin
    // Change - to separator items
    hm := GetMenu(NppData.NppHandle);
    for i := 0 to Length(FuncArray)-1 do
      if (FuncArray[i].ItemName[0] = '-') then
        ModifyMenu(hm, FuncArray[i].CmdID, MF_BYCOMMAND or MF_SEPARATOR, 0, nil);
  end;
  Dispatch(Msg);
end;

procedure TNppPlugin.SetMenuItemCheck(CmdId: DWORD; Check: boolean);
begin
  SendToNpp(NPPM_SETMENUITEMCHECK, CmdID, DWORD(check));
end;

procedure TNppPlugin.SetToolbarIcon(out pToolbarIcon: TToolbarIcons);
begin
  // To be overridden for customization
end;

procedure TNppPlugin.SetInfo(pNppData: TNppData);
begin
  FNppData := pNppData;
end;

function TNppPlugin.DoOpen(filename: nppString): boolean;
var
  s: nppString;
begin
  // ask if we are not already opened
  SetLength(s, 500);
  SendToNpp(NPPM_GETFULLCURRENTPATH, 0, LPARAM(nppPChar(s)));
  SetString(s, nppPChar(s), strlen(nppPChar(s)));
  Result := true;
  if s = filename then exit;

  Result := SendToNpp(NPPM_DOOPEN, 0, LPARAM(nppPChar(filename))) = 1;
end;

function TNppPlugin.DoOpen(filename: nppString; Line: Integer): boolean;
begin
  result := DoOpen(filename);
  if result then
    SendToNppScintilla(SciSupport.SCI_GOTOLINE, Line, 0);
end;

// overrides 
procedure TNppPlugin.DoNppnShutdown;
begin
  // override these
end;

function TNppPlugin.GetStringNPP(pMsg: UInt; Len: DWORD = 4096): nppString;
var
  s: nppString;
  i: integer;
begin
  Result := '';
  SetLength(s, Len+1);
  SendToNpp(pMsg, Len, LPARAM(nppPChar(s)));
  for i:=1 to Len+1 do
    if s[i]=#0 then
    begin
      SetLength(s, i-1);
      Result := s;
      break;
    end;
end;

function TNppPlugin.GetStringSCI(pMsg: UInt; Len: DWORD): nppString;
var
  s: UTF8String;
  i: integer;
begin
  Result := '';
  SetLength(s, Len+1);
  SendToNppScintilla(pMsg, Len, LPARAM(PAnsiChar(s)));
  for i:=1 to Len+1 do
    if s[i]=#0 then
    begin
      SetLength(s, i-1);
      Result := UTF8Decode(s);
      break;
    end;
end;

procedure TNppPlugin.DoNppnToolbarModification;
var
  tb: TToolbarIcons;
begin
  tb.ToolbarIcon := 0;
  tb.ToolbarBmp  := 0;
  SetToolbarIcon(tb);
  if (tb.ToolbarBmp <> 0) or (tb.ToolbarIcon <> 0) then
    SendToNpp(NPPM_ADDTOOLBARICON, WPARAM(CmdIdFromDlgId(1)), LPARAM(@tb));
end;

function TNppPlugin.CmdIdFromDlgId(DlgId: Integer): Integer;
begin
  Result := FuncArray[DlgId].CmdId;
end;

//////////// EXAMPLE CODE: //////////////////
(*procedure TNppPlugin.GetFileLine(var filename: nppString; var Line: Integer);
var
  s: nppString;
  r: integer;
begin
  s := '';
  SetLength(s, 300);
  SendToNpp(NPPM_GETFULLCURRENTPATH,0, LPARAM(nppPChar(s)));
  SetLength(s, StrLen(nppPChar(s)));
  filename := s;
  r    := SendToNppScintilla(SciSupport.SCI_GETCURRENTPOS, 0, 0);
  Line := SendtoNppScintilla(SciSupport.SCI_LINEFROMPOSITION, r, 0);
end;

function TNppPlugin.GetWord: nppString;
begin
  result := GetString(NPPM_GETCURRENTWORD,800);
end;

*)

end.
