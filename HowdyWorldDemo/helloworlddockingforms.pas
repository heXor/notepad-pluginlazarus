unit helloworlddockingforms;

{$MODE Delphi}

interface

uses
  LCLIntf, LCLType, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls, NppPlugin, NppDockingForms, NppTypes;

type

  { THelloWorldDockingForm }

  THelloWorldDockingForm = class(TNppDockingForm)
    Button1: TButton;
    Button2: TButton;
    Memo1: TMemo;
    Panel1: TPanel;
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure FormHide(Sender: TObject);
    procedure FormFloat(Sender: TObject);
    procedure FormDock(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

implementation

{$R *.lfm}

procedure THelloWorldDockingForm.FormCreate(Sender: TObject);
begin
  NppDefaultDockingMask := DWS_DF_FLOATING; // whats the default docking position
  KeyPreview            := true; // special hack for input forms
  OnFloat               := FormFloat;
  OnDock                := FormDock;
  inherited;
end;

procedure THelloWorldDockingForm.Button1Click(Sender: TObject);
begin
  inherited;
  UpdateDisplayInfo('Test window');
end;

procedure THelloWorldDockingForm.Button2Click(Sender: TObject);
begin
  inherited;
  Hide;
end;

// special hack for input forms
// This is the best possible hack I could came up for
// memo boxes that don't process enter keys for reasons
// too complicated... Has something to do with Dialog Messages
// I sends a Ctrl+Enter in place of Enter
procedure THelloWorldDockingForm.FormKeyPress(Sender: TObject;  var Key: Char);
begin
  inherited;
  if (Key = #13) and (self.Memo1.Focused) then
    Memo1.Perform(WM_CHAR, 10, 0);
end;

// Docking code calls this when the form is hidden by either "x" or self.Hide
procedure THelloWorldDockingForm.FormHide(Sender: TObject);
begin
  inherited;
  GetNPPPluginInstance.SendToNpp(NPPM_SETMENUITEMCHECK, self.CmdID, 0);
end;

procedure THelloWorldDockingForm.FormDock(Sender: TObject);
begin
  GetNPPPluginInstance.SendToNpp(NPPM_SETMENUITEMCHECK, self.CmdID, 1);
end;

procedure THelloWorldDockingForm.FormFloat(Sender: TObject);
begin
  GetNPPPluginInstance.SendToNpp(NPPM_SETMENUITEMCHECK, self.CmdID, 1);
end;

procedure THelloWorldDockingForm.FormShow(Sender: TObject);
begin
  inherited;
  GetNPPPluginInstance.SendToNpp(NPPM_SETMENUITEMCHECK, self.CmdID, 1);
end;

end.
